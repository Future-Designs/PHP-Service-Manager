<?php
/**
 * @copyright Copyright (c) 2018 TraSo GmbH (www.traso.de)
 * @author j.lohse
 * @since 04.06.18
 */

namespace Traso\XIBE\ServiceManager;

/**
 * Class InvokableFactory
 * @package Traso\XIBE\ServiceManager
 */
class InvokableFactory
{
    /**
     * @param string $class
     * @return mixed
     */
    public function __invoke(string $class)
    {
        return new $class();
    }
}