<?php
/**
 * @copyright Copyright (c) 2018 TraSo GmbH (www.traso.de)
 * @author j.lohse
 * @since 04.06.18
 */

namespace Traso\XIBE\ServiceManager;

/**
 * Interface ServiceManagerInterface
 * @package Traso\XIBE\ServiceManager
 */
interface ServiceManagerInterface
{
    public function get(string $class);
    public function getFromConfig(array $args);
}